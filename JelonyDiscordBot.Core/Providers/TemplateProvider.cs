﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Emgu.CV;
using Emgu.CV.CvEnum;

namespace JelonyDiscordBot.Core.Providers
{
    public interface ITemplateProvider
    {
        List<Mat> ReadTemplates();
        void AddTemplate(IInputArray image);
    }
    
    public class TemplateProvider : ITemplateProvider
    {
        private const string TemplatesDirectory = "templates";
        private readonly object _lock = new object();
        private int _currentTemplateId;

        public TemplateProvider()
        {
            Directory.CreateDirectory(TemplatesDirectory);
            var templates = Directory.GetFiles(TemplatesDirectory);
            _currentTemplateId = 0;
            if (templates.Length != 0)
                _currentTemplateId = templates.Max(t => int.TryParse(Regex.Match(t, @"\d+").Value, out var id) ? id : 0) + 1;
        }

        public List<Mat> ReadTemplates()
        {
            lock (_lock)
            {
                var result = new List<Mat>();
                foreach (var template in Directory.EnumerateFiles(TemplatesDirectory))
                    result.Add(ReadTemplate(template));
                return result;
            }
        }

        public void AddTemplate(IInputArray image)
        {
            lock (_lock)
            {
                var nextTemplatePath = $"{TemplatesDirectory}\\template{++_currentTemplateId}.png";
                CvInvoke.Imwrite(nextTemplatePath, image);
            }
        }

        private Mat ReadTemplate(string path)
        {
            var result = CvInvoke.Imread(path);
            CvInvoke.CvtColor(result, result, ColorConversion.Bgr2Gray);
            CvInvoke.Canny(result, result, 50, 200);
            return result;
        }
    }
}
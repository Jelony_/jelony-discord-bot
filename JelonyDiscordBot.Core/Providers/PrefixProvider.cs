﻿using System.Collections.Generic;

namespace JelonyDiscordBot.Core.Providers
{
    public interface IPrefixProvider
    {
        string GetPrefix(ulong guildId);
        void SetPrefix(ulong guildId, string prefix);
    }

    public class PrefixProvider : IPrefixProvider
    {
        private readonly Dictionary<ulong, string> _definedPrefixes;
        private readonly object _lock;

        public PrefixProvider()
        {
            _definedPrefixes = new Dictionary<ulong, string>();
            _lock = new object();
        }

        public string GetPrefix(ulong guildId)
        {
            lock (_lock)
                return _definedPrefixes.TryGetValue(guildId, out var prefix) ? prefix : null;
        }

        public void SetPrefix(ulong guildId, string prefix)
        {
            lock (_lock)
                _definedPrefixes[guildId] = prefix;
        }
    }
}
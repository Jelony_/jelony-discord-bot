﻿using System;
using System.IO;

namespace JelonyDiscordBot.Core.Providers
{
    public interface IJobIdProvider : IDisposable
    {
        int GetNextJobId();
        string GetDirectoryForId(int jobId);
    }

    public class JobIdProvider : IJobIdProvider
    {
        private const string JobsDirectory = "temp";

        private readonly object _lock = new object();
        private int _nextJobId;

        public JobIdProvider()
        {
            CleanUp();
            _nextJobId = 1;
            Directory.CreateDirectory(JobsDirectory);
        }

        public int GetNextJobId()
        {
            lock (_lock)
            {
                return _nextJobId++;
            }
        }

        public string GetDirectoryForId(int jobId)
        {
            return Path.Combine(JobsDirectory, jobId.ToString());
        }

        public void Dispose()
        {
            CleanUp();
        }

        private void CleanUp()
        {
            if (Directory.Exists(JobsDirectory))
                Directory.Delete(JobsDirectory, true);
        }
    }
}
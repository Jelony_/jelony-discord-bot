﻿using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using JelonyDiscordBot.Core.Modules;
using JelonyDiscordBot.Core.Providers;
using JelonyDiscordBot.Core.Services;
using JelonyDiscordBot.Shared.Options;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace JelonyDiscordBot.Core
{
    public class CommandHandler
    {
        private readonly ILogger _logger;
        private readonly IOptions<OtherOptions> _options;
        private readonly CommandService _commandService;
        private readonly DiscordSocketClient _client;
        private readonly IServiceProvider _serviceProvider;
        private readonly IPrefixProvider _prefixProvider;
        private readonly INonCommandMessageHandler _nonCommandMessageHandler;
        private DateTime _lastCommandUtc;

        public CommandHandler(
            ILogger<CommandHandler> logger,
            IOptions<OtherOptions> options,
            DiscordSocketClient client, 
            CommandService commands,
            IServiceProvider serviceProvider,
            IPrefixProvider prefixProvider,
            INonCommandMessageHandler nonCommandMessageHandler)
        {
            _logger = logger;
            _options = options;
            _commandService = commands;
            _client = client;
            _serviceProvider = serviceProvider;
            _prefixProvider = prefixProvider;
            _nonCommandMessageHandler = nonCommandMessageHandler;
        }

        public async Task InstallCommandsAsync()
        {
            _logger.LogInformation("Initializing commands");
            _client.MessageReceived += HandleCommandAsync;
            _client.Log += Log;
            _commandService.CommandExecuted += CommandExecutedAsync;

            var assembly = typeof(CommandHandler).Assembly;
            _logger.LogInformation($"Assembly = {assembly}");
            await _commandService.AddModulesAsync(assembly, _serviceProvider);
        }

        private async Task HandleCommandAsync(SocketMessage messageParam)
        {
            var message = messageParam as SocketUserMessage;
            if (message?.Channel is SocketGuildChannel channel)
                await HandleChannelCommandAsync(message, channel);
            else if (message?.Channel is ISocketPrivateChannel privateChannel)
                await HandlePrivateCommandAsync(message, privateChannel);
        }

        private async Task HandleChannelCommandAsync(SocketUserMessage message, SocketGuildChannel channel)
        {
            var now = DateTime.UtcNow;

            var context = new SocketCommandContext(_client, message);

            var prefix = _prefixProvider.GetPrefix(channel.Guild.Id);

            int argPos = 0;
            if (!(prefix == null ||
                  message.HasStringPrefix(prefix, ref argPos) ||
                  message.HasMentionPrefix(_client.CurrentUser, ref argPos)) ||
                message.Author.IsBot)
                return;

            if (now - _lastCommandUtc < _options.Value.MessageUserDelay)
                return;

            _lastCommandUtc = now;

            if (message.Attachments.Count != 0)
            {
                var images = message.Attachments.Where(x =>
                    x.Filename.EndsWith(".jpg") || x.Filename.EndsWith(".png") ||
                    x.Filename.EndsWith(".gif")).ToList();
                if (images.Count != 0)
                {
                    var templateRemoveModule = _serviceProvider.GetService(typeof(TemplateRemoveModule)) as TemplateRemoveModule;
                    await templateRemoveModule.RemoveTemplate(context, images);
                }
            }

            await _commandService.ExecuteAsync(
                context: context,
                argPos: argPos,
                _serviceProvider);
        }

        private async Task HandlePrivateCommandAsync(SocketUserMessage message, ISocketPrivateChannel channel)
        {
            var context = new SocketCommandContext(_client, message);

            if (message.Author.IsBot)
                return;

            await _commandService.ExecuteAsync(
                context: context,
                argPos: 0,
                _serviceProvider);
        }

        private async Task CommandExecutedAsync(Optional<CommandInfo> command, ICommandContext context, IResult result)
        {
            if (context.Channel is ISocketPrivateChannel)
                return;

            if (!result.IsSuccess)
                await _nonCommandMessageHandler.Handle(context);

            var prefix = _prefixProvider.GetPrefix(context.Guild.Id);
            if (prefix == null) return;

            if (!string.IsNullOrEmpty(result?.ErrorReason))
                await context.Channel.SendMessageAsync("co ty odpierdalasz xd");

            var commandName = command.IsSpecified ? command.Value.Name : "A command";
            _logger.LogInformation($"{commandName} was executed at {DateTime.UtcNow}.");
        }

        private Task Log(LogMessage message)
        {
            _logger.LogInformation(message.ToString());
            return Task.CompletedTask;
        }
    }
}
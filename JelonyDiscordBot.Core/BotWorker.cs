﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Discord.WebSocket;
using JelonyDiscordBot.Core.Services;
using JelonyDiscordBot.Shared.Options;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace JelonyDiscordBot.Core
{
    public class BotWorker : IHostedService, IDisposable
    {
        private static TimeSpan Delay = TimeSpan.FromMinutes(5);

        private readonly ILogger<BotWorker> _logger;
        private readonly IVotedUsersService _votedUsersService;
        private readonly DiscordSocketClient _client;
        private readonly IOptions<OtherOptions> _options;
        private Timer _timer;

        public BotWorker(ILogger<BotWorker> logger,
            IOptions<OtherOptions> options,
            DiscordSocketClient client,
            IVotedUsersService votedUsersService)
        {
            _logger = logger;
            _client = client;
            _options = options;
            _votedUsersService = votedUsersService;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Bot worker started");

            _timer = new Timer(DoWork, null, TimeSpan.Zero, Delay);

            return Task.CompletedTask;
        }

        private async void DoWork(object state)
        {
            var votingTimeZone = TimeZoneInfo.FindSystemTimeZoneById(_options.Value.VotingServerTimeZone);
            var votingTime = TimeZoneInfo.ConvertTime(DateTime.UtcNow, votingTimeZone);
            if (votingTime.TimeOfDay < Delay)
            {
                var user = _votedUsersService.GetMostVoted();
                var channel = await _client.GetChannelAsync(_options.Value.VotingChannel) as ISocketMessageChannel;
                if (channel == null)
                    return;
                await channel.SendMessageAsync(
                    $"najgorszym uzytkownikiem dzisiaj zostal {user.Mention}. popraw sie albo bana dostaniesz!!");
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Bot worker stopping...");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
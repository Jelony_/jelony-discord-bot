﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using JelonyDiscordBot.Data;
using JelonyDiscordBot.Shared.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;

namespace JelonyDiscordBot.Core
{
    public class BotClient : IHostedService, IDisposable
    {
        private readonly DiscordSocketClient _client;
        private readonly IOptions<DiscordOptions> _config;
        private readonly CommandHandler _commandHandler;

        public BotClient(DiscordSocketClient client,
            IOptions<DiscordOptions> config,
            CommandHandler commandHandler)
        {
            _client = client;
            _config = config;
            _commandHandler = commandHandler;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await _client.LoginAsync(TokenType.Bot, _config.Value.Token);
            await _client.StartAsync();
            await _commandHandler.InstallCommandsAsync();

            using var db = new JelonyBotContext();
            await db.Database.MigrateAsync(cancellationToken);
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await _client.StopAsync();
        }

        public void Dispose()
        {
            if (_client != null)
            {
                _client.StopAsync();
                _client.Dispose();
            }
        }
    }
}
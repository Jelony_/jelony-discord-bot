﻿using System.Text;

namespace JelonyDiscordBot.Core.Extensions
{
    public static class StringExtensions
    {
        public static string RemovePolishCharacters(this string value)
        {
            var stringBuilder = new StringBuilder();
            foreach (var character in value)
                stringBuilder.Append(NormalizeChar(character));
            return stringBuilder.ToString();
        }

        private static char NormalizeChar(char c)
        {
            switch (c)
            {
                case 'ą':
                    return 'a';
                case 'ć':
                    return 'c';
                case 'ę':
                    return 'e';
                case 'ł':
                    return 'l';
                case 'ń':
                    return 'n';
                case 'ó':
                    return 'o';
                case 'ś':
                    return 's';
                case 'ż':
                case 'ź':
                    return 'z';
            }
            return c;
        }
    }
}
﻿using System;
using System.Collections.Generic;

namespace JelonyDiscordBot.Core.Services
{
    public interface ISlotsService
    {
        void DelayUser(ulong id);
        bool IsDelayed(ulong id);
        void ChangeDelay(TimeSpan delay);
        void ChangeChance(float multiplier);
        int Slot();
    }
    public class SlotsService : ISlotsService
    {
        private readonly Random _random = new Random();
        private readonly Dictionary<ulong, DateTime> _users = new Dictionary<ulong, DateTime>();
        private TimeSpan _delay = TimeSpan.FromDays(1);
        private int _chance = 20;

        public void DelayUser(ulong id)
        {
            _users[id] = DateTime.UtcNow;
        }

        public bool IsDelayed(ulong id)
        {
            var now = DateTime.UtcNow;
            return _users.ContainsKey(id) &&
                   now - _users[id] < _delay;
        }

        public void ChangeDelay(TimeSpan delay)
        {
            _delay = delay;
        }

        public void ChangeChance(float multiplier)
        {
            _chance = (int)(_chance / multiplier);
        }

        public int Slot()
        {
            if (_random.Next(_chance) == 0)
            {
                if (_random.Next(_chance) == 0)
                    return 3;
                return 2;
            }

            return 1;
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord.Commands;
using Discord.WebSocket;
using JelonyDiscordBot.Shared.Options;
using Microsoft.Extensions.Options;

namespace JelonyDiscordBot.Core.Services
{
    public interface IVotedUsersService
    {
        Task<bool> Vote(SocketCommandContext context, string username);
        SocketGuildUser GetMostVoted();
    }

    public class VotedUsersService : IVotedUsersService
    {
        private readonly IOptions<OtherOptions> _options;
        private readonly Dictionary<SocketGuildUser, int> _votes = new Dictionary<SocketGuildUser, int>();
        private readonly HashSet<SocketUser> _votingUsers = new HashSet<SocketUser>();
        private readonly object _lock = new object();

        public VotedUsersService(IOptions<OtherOptions> options)
        {
            _options = options;
        }

        public async Task<bool> Vote(SocketCommandContext context, string username)
        {
            lock (_lock)
            {
                if (_votingUsers.Contains(context.User))
                    return false;
                _votingUsers.Add(context.User);
            }

            var userToCompare = username.ToLowerInvariant();
            var guild = context.Client.GetGuild(_options.Value.VotingServer);
            await guild.DownloadUsersAsync();
            var user = guild.Users.FirstOrDefault(u => u.Username.ToLowerInvariant().Contains(userToCompare));

            if (user == null)
                return false;

            lock (_lock)
            {
                if (!_votes.ContainsKey(user))
                    _votes[user] = 0;
                _votes[user]++;
            }

            return true;
        }

        public SocketGuildUser GetMostVoted()
        {
            lock (_lock)
            {
                var mostVotedUser = _votes.OrderByDescending(a => a.Value).FirstOrDefault();
                if (_votes.Values.Count(a => a == mostVotedUser.Value) > 1)
                    return null;
                _votes.Clear();
                _votingUsers.Clear();
                return mostVotedUser.Key;
            }
        }
    }
}
﻿using System;
using System.Drawing;
using System.IO;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using JelonyDiscordBot.Core.Providers;
using JelonyDiscordBot.Shared.Options;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace JelonyDiscordBot.Core.Services
{
    public interface ITemplateRemover : IDisposable
    {
        string InputDirectory { get; }
        string OutputDirectory { get; }
        void Process();
    }

    public class TemplateRemover : ITemplateRemover
    {
        private const string InputDirectoryName = "input";
        private const string OutputDirectoryName = "output";

        private readonly ILogger _logger;
        private readonly ITemplateProvider _templateProvider;
        private readonly IOptions<TemplateRemoverOptions> _config;

        private readonly string _jobDirectory;

        public string InputDirectory { get; }
        public string OutputDirectory { get; }

        public TemplateRemover(
            ILogger<TemplateRemover> logger,
            ITemplateProvider templateProvider,
            IJobIdProvider jobIdProvider,
            IOptions<TemplateRemoverOptions> config)
        {
            _logger = logger;
            var jobId = jobIdProvider.GetNextJobId();
            _jobDirectory = jobIdProvider.GetDirectoryForId(jobId);
            _templateProvider = templateProvider;
            _config = config;
            InputDirectory = Path.Combine(_jobDirectory, InputDirectoryName);
            OutputDirectory = Path.Combine(_jobDirectory, OutputDirectoryName);

            Directory.CreateDirectory(InputDirectory);
            Directory.CreateDirectory(OutputDirectory);
        }

        public void Process()
        {
            _logger.LogInformation($"Processing images in {_jobDirectory}");
            var templates = _templateProvider.ReadTemplates();
            var config = _config.Value;

            foreach (var file in Directory.EnumerateFiles(InputDirectory))
            {
                var fullImage = CvInvoke.Imread(file);
                var offset = new Point(fullImage.Width - config.MaxTemplateWidth, fullImage.Height - config.MaxTemplateHeight);
                var image = new Mat(fullImage, new Rectangle(offset.X, offset.Y, config.MaxTemplateWidth, config.MaxTemplateHeight));
                var gray = new Mat();
                CvInvoke.CvtColor(image, gray, ColorConversion.Bgr2Gray);
                CvInvoke.Canny(gray, gray, 50, 200);

                var maxMaxVal = 0d;
                Point maxStart = default;
                Point maxEnd = default;

                foreach (var template in templates)
                {
                    var result = new Mat();
                    CvInvoke.MatchTemplate(gray, template, result, TemplateMatchingType.CcoeffNormed);
                    double minVal = 0d, maxVal = 0d;
                    Point minLoc = default, maxLoc = default;
                    CvInvoke.MinMaxLoc(result, ref minVal, ref maxVal, ref minLoc, ref maxLoc);
                    if (maxVal > maxMaxVal)
                    {
                        maxMaxVal = maxVal;
                        maxStart = maxLoc;
                        maxEnd = new Point(maxLoc.X + template.Width, maxLoc.Y + template.Height);
                    }
                }

                _logger.LogInformation($"Result for file {file}: {maxMaxVal}. Minimum is {config.ValLimit}");
                if (maxMaxVal > config.ValLimit)
                {
                    CvInvoke.Rectangle(fullImage, new Rectangle(
                        maxStart.X + offset.X, maxStart.Y + offset.Y,
                        maxEnd.X - maxStart.X, maxEnd.Y - maxStart.Y), new MCvScalar(0, 0, 255), -1);

                    var newFile = Path.Combine(OutputDirectory, $"{(int) (maxMaxVal * 10000)}_{Path.GetFileName(file)}");
                    CvInvoke.Imwrite(newFile, fullImage);
                    _logger.LogInformation($"Saving {newFile}. Succeeded: {File.Exists(newFile)}");
                }
            }

            _logger.LogInformation($"Processed images in {_jobDirectory}");
        }

        public void Dispose()
        {
            if (Directory.Exists(_jobDirectory))
                Directory.Delete(_jobDirectory, true);
        }
    }
}
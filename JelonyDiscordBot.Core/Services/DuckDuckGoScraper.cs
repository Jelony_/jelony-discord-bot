﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;

namespace JelonyDiscordBot.Core.Services
{
    public class DuckDuckGoScraper
    {
        private const string Url = "https://www.duckduckgo.com/";
        private readonly Dictionary<string, string> _headers = new Dictionary<string, string>
        {
            { "accept", "application/json, text/javascript, */*; q=0.01" },
            { "user-agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36" },
            { "referer", "https://duckduckgo.com/" },
            { "authority", "duckduckgo.com" },
            { "sec-fetch-dest", "empty" },
            { "x-requested-with", "XMLHttpRequest" },
            { "sec-fetch-site", "same-origin" },
            { "sec-fetch-mode", "cors" },
            { "accept-language", "en-US,en;q=0.9" }
        };
        private Dictionary<string, string> GetParams(string topic) =>
            new Dictionary<string, string>
            {
                { "q", topic },
                { "vqd", GetToken(topic) },
                { "l", "us-en" },
                { "o", "json" },
                { "f", ",,," },
                { "p", "-1" }
            };

        public Stream GetRandomImage(string key)
        {
            var json = GetJsonWithImages(key);

            if (json == null)
                return null;

            List<string> urls = GetUrls(json);

            if (urls == null || urls.Count == 0)
                return null;

            var rnd = new Random();
            int randomUrl = rnd.Next(0, urls.Count - 1);
            string luckyUrl = urls[randomUrl];

            byte[] image = GetImage(luckyUrl);
            if (image == null)
                return null;

            return new MemoryStream(image);
        }

        private byte[] GetImage(string url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            var response = (HttpWebResponse)request.GetResponse();

            using Stream dataStream = response.GetResponseStream();
            if (dataStream == null)
                return null;
            using var sr = new BinaryReader(dataStream);
            byte[] bytes = sr.ReadBytes(100000000);

            return bytes;
        }

        private List<string> GetUrls(JObject json)
        {
            var results = json["results"];
            return results?.Children().Where(a => a["image"] != null).Select(a => a["image"].ToString()).ToList();
        }

        private JObject GetJsonWithImages(string topic)
        {
            string url = Url + "i.js?" + string.Join('&', GetParams(topic).Select(a => $"{a.Key}={a.Value}"));
            string data = "";

            var request = (HttpWebRequest)WebRequest.Create(url);
            foreach (var header in _headers)
                request.Headers.Add(header.Key, header.Value);

            var response = (HttpWebResponse)request.GetResponse();

            using Stream dataStream = response.GetResponseStream();
            if (dataStream == null)
                return null;

            using var sr = new StreamReader(dataStream);
            data = sr.ReadToEnd();

            return JObject.Parse(data);
        }

        private string GetToken(string topic)
        {
            string url = Url + "?q=" + topic;
            var request = (HttpWebRequest)WebRequest.Create(url);
            var response = (HttpWebResponse)request.GetResponse();

            using Stream dataStream = response.GetResponseStream();
            if (dataStream == null)
                return "";
            using var sr = new StreamReader(dataStream);
            var data = sr.ReadToEnd();

            var match = Regex.Match(data, @"vqd=([\d-]+)\&");
            return match.Groups[1].Value;
        }


    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Discord.Commands;
using JelonyDiscordBot.Core.Extensions;

namespace JelonyDiscordBot.Core.Services
{
    public interface INonCommandMessageHandler
    {
        Task Handle(ICommandContext message);
    }

    public class NonCommandMessageHandler : INonCommandMessageHandler
    {
        private const string SentencesFilePath = "sentences.txt";
        private readonly Dictionary<string, List<string>> _rhymeSentences;
        private readonly Random _random;

        public NonCommandMessageHandler()
        {
            _random = new Random();
            if (!File.Exists(SentencesFilePath))
                return;

            var sentences = File.ReadAllLines(SentencesFilePath);
            _rhymeSentences = new Dictionary<string, List<string>>();

            for (int i = 0; i < sentences.Length; i += 2)
            {
                AddSentence(sentences[i], sentences[i + 1]);
                AddSentence(sentences[i + 1], sentences[i]);
            }
        }

        public async Task Handle(ICommandContext context)
        {
            if (_rhymeSentences == null)
                return;

            var messageLastWord = context.Message.Content
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Last()
                .ToLowerInvariant()
                .RemovePolishCharacters();

            var rhymes = _rhymeSentences[messageLastWord];
            var randomChosenRhyme = rhymes[_random.Next(rhymes.Count)];
            await context.Channel.SendMessageAsync(randomChosenRhyme);
        }

        private void AddSentence(string sentence1, string sentence2)
        {
            var lastWord = sentence1
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Last()
                .ToLowerInvariant()
                .RemovePolishCharacters();

            if (!_rhymeSentences.TryGetValue(lastWord, out var rhymesToWord))
            {
                rhymesToWord = new List<string>();
                _rhymeSentences.Add(lastWord, rhymesToWord);
            }
            rhymesToWord.Add(sentence2);
        }
    }
}
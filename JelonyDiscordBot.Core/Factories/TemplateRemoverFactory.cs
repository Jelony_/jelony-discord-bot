﻿using System;
using JelonyDiscordBot.Core.Services;

namespace JelonyDiscordBot.Core.Factories
{
    public interface ITemplateRemoverFactory
    {
        ITemplateRemover Create();
    }

    public class TemplateRemoverFactory : ITemplateRemoverFactory
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly object _lock = new object();

        public TemplateRemoverFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public ITemplateRemover Create()
        {
            lock (_lock)
                return (ITemplateRemover)_serviceProvider.GetService(typeof(ITemplateRemover));
        }
    }
}
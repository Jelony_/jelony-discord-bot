﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using JelonyDiscordBot.Data;
using JelonyDiscordBot.Data.Entity;

namespace JelonyDiscordBot.Core.Modules
{
    [Group("cipka"), RequireContext(ContextType.Guild)]
    public class CipkaModule : ModuleBase<SocketCommandContext>
    {
        private readonly Random _random = new Random();

        [Command]
        public async Task CipkaAsync()
        {
            using var db = new JelonyBotContext();

            var disabledChannel = await db.CipkaDisabledChannels.FirstOrDefaultAsync(a => a.ChannelId == Context.Channel.Id);
            if (disabledChannel != null)
                return;

            var it = _random.Next(await db.Cipkas.CountAsync() - 1);
            var cipka = await db.Cipkas.ElementAtAsync(it);

            await Context.Channel.SendMessageAsync(cipka.Name);
        }

        [RequireOwner]
        [Command("dodaj")]
        public async Task DodajCipkeAsync(string newCipka)
        {
            using var db = new JelonyBotContext();
            await db.Cipkas.AddAsync(new Cipka {Name = newCipka});
            await db.SaveChangesAsync();
            
            await Context.Channel.SendMessageAsync($"znam juz {await db.Cipkas.CountAsync()} cipek pog");
        }

        [RequireOwner]
        [Command("usun")]
        public async Task UsunCipkeAsync(string cipkaName)
        {
            using var db = new JelonyBotContext();
            var cipka = await db.Cipkas.FirstOrDefaultAsync(a => a.Name == cipkaName);
            if (cipka == null)
            {
                await Context.Channel.SendMessageAsync("nie ma takiej cipewy");
                return;
            }

            db.Cipkas.Remove(cipka);
            await db.SaveChangesAsync();
            
            await Context.Channel.SendMessageAsync("cipka usunieta");
        }

        [RequireUserPermission(GuildPermission.ManageChannels)]
        [Command("wyloncz")]
        public async Task DisableCipkaAsync()
        {
            using var db = new JelonyBotContext();
            await db.CipkaDisabledChannels.AddAsync(new CipkaDisabledChannel {ChannelId = Context.Channel.Id});
            await db.SaveChangesAsync();

            await Context.Channel.SendMessageAsync("cipki wylonczone na tym kanale");
        }

        [RequireUserPermission(GuildPermission.ManageChannels)]
        [Command("wloncz")]
        public async Task EnableCipkaAsync()
        {
            using var db = new JelonyBotContext();
            var channel = await db.CipkaDisabledChannels.FirstOrDefaultAsync(a => a.ChannelId == Context.Channel.Id);
            if (channel == null)
                return;

            db.CipkaDisabledChannels.Remove(channel);
            await db.SaveChangesAsync();

            await Context.Channel.SendMessageAsync("cipki wlonczone na tym kanale");
        }
    }
}
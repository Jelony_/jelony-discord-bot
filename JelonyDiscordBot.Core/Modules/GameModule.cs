﻿using System.Threading.Tasks;
using Discord.Commands;
using JelonyDiscordBot.Core.Services;

namespace JelonyDiscordBot.Core.Modules
{
    public class GameModule : ModuleBase<SocketCommandContext>
    {
        private readonly IVotedUsersService _votedUsersService;

        public GameModule(IVotedUsersService votedUsersService)
        {
            _votedUsersService = votedUsersService;
        }

        [Command("glosuj"), RequireContext(ContextType.DM)]
        public async Task Glosuj(string user)
        {
            if (!Context.IsPrivate)
                return;

            var wasVoted = await _votedUsersService.Vote(Context, user);

            if (!wasVoted)
                await Context.Channel.SendMessageAsync($"nie znaleziono typa {user} albo juz glosowales");
            else
                await Context.Channel.SendMessageAsync("zaglosowane");
        }
    }
}
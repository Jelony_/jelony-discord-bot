﻿using System;
using System.Threading.Tasks;
using Discord.Commands;

namespace JelonyDiscordBot.Core.Modules
{
    [RequireContext(ContextType.Guild)]
    public class RandomValueModule : ModuleBase<SocketCommandContext>
    {
        private readonly Random _random = new Random();

        private readonly string[] _randomTexts =
        {
            "zostal wyrzucony randomkickiem",
            "powonchal wyderke i smierdziala fuj",
            "prowadzi eksterminacje z montesem",
            "rozbija szklany stul z kamaro",
            "jest glodny bo pajonk zjadl cale kfc",
            "uciekl przed starym emci",
            "odkryl kielce i zaginal w dziwnych okolicznosciach",
            "odkrywa nowe mity z trytkom",
            "przytula panterke",
            "robi przeglond cipek z jelonym",
            "spendza czas w arkadii z bazingom",
            "oglonda furry porno",
            "idzie na nocke do amazonu z agnaxem",
            "buduje schron atomowy z dżokerem",
            "ucieka przed oposem bo znowu krzyczy",
            "mial piekny pogrzeb",
            "ma zakola jak zatoka gdanska",
            "wkurwil kamaro szczepiac sie na kowid",
            "nieironicznie zazartowal z pajonka",
            "zjadl calom faksidiete i umarl na cukrzyce",
            "nigdy nie bedzie kobieta",
            "po prostu zamknij juz morde blagam",
            "zbiera na wstawienie proguw do auta",
            "simpi do saltiego",
            "to big czungus",
            "posiada zdjencia dupy incela na pw",
            "pozyczyl stuwke pajonkowi",
            "smiesza go fajne przerubki, w sensie memy",
            "rozmawia z meblami"
        };

        [Command("lego")]
        private async Task GetLegoTowerHeight()
        {
            var height = _random.Next(251);
            var result = 
                height < 10 ? "to niezla wieza xd" :
                height < 100 ? "calkiem durza" :
                height < 200 ? "ale ogromna o:" : "ale bydle!!!! O:";
            await Context.Channel.SendMessageAsync($"{Context.User.Mention} ulozyl wieze z klockuw lego o wysokosci {height} klocuszkuw. {result}");
        }

        [Command("jelony")]
        private async Task GetJelonyLikeRatio()
        {
            var like = _random.Next(101);
            var result = 
                like < 10 ? "popraw sie prosze" :
                like < 40 ? "no niech bedzie" :
                like < 70 ? "jestes lubiany :3" : "omg jelony cie kocha!!";
            await Context.Channel.SendMessageAsync($"{Context.User.Mention}. jelony cie lubi na {like}%. {result}");
        }

        [Command("co")]
        private async Task GetRandomText()
        {
            var result = _randomTexts[_random.Next(_randomTexts.Length)];
            await Context.Channel.SendMessageAsync($"{Context.User.Mention} {result}");
        }
    }
}
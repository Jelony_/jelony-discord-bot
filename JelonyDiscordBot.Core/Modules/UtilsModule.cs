﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;

namespace JelonyDiscordBot.Core.Modules
{
    [RequireContext(ContextType.Guild)]
    public class UtilsModule : ModuleBase<SocketCommandContext>
    {
        [Command("co ty robisz")]
        public async Task UndoAsync()
        {
            var repliedMsg = Context.Message.ReferencedMessage;

            if (repliedMsg.Author.Id != Context.Client.CurrentUser.Id)
                return;

            await repliedMsg.DeleteAsync();
            await Context.Channel.SendMessageAsync("sory");
        }
    }
}
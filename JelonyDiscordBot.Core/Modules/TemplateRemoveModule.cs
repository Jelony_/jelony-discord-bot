﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using JelonyDiscordBot.Core.Factories;

namespace JelonyDiscordBot.Core.Modules
{
    public class TemplateRemoveModule
    {
        private readonly ITemplateRemoverFactory _templateRemoverFactory;

        public TemplateRemoveModule(ITemplateRemoverFactory templateRemoverFactory)
        {
            _templateRemoverFactory = templateRemoverFactory;
        }

        public async Task RemoveTemplate(SocketCommandContext context, List<Attachment> images)
        {
            using var templateRemover = _templateRemoverFactory.Create();
            using var webClient = new WebClient();

            foreach (var attachment in images)
                webClient.DownloadFile(attachment.Url,
                    Path.Combine(templateRemover.InputDirectory, attachment.Filename));

            templateRemover.Process();

            var result = Directory.EnumerateFiles(templateRemover.OutputDirectory).ToList();
            
            if (result.Count > 0)
            {
                foreach (var file in result)
                {

                    var message = $"{context.User.Mention} to debil jebany";
                    await context.Channel.SendFileAsync(file, message);
                }

                await context.Message.DeleteAsync();
            }
        }
    }
}
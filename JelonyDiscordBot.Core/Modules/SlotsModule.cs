﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using JelonyDiscordBot.Core.Services;

namespace JelonyDiscordBot.Core.Modules
{
    [RequireContext(ContextType.Guild)]
    public class SlotsModule : ModuleBase<SocketCommandContext>
    {
        private readonly Random _random = new Random();
        private readonly ISlotsService _slotsService;

        public SlotsModule(ISlotsService slotsService)
        {
            _slotsService = slotsService;
        }

        [Command("losuj")]
        public async Task LosujAsync()
        {
            if (_slotsService.IsDelayed(Context.User.Id))
            {
                await Context.Channel.SendMessageAsync("przystopuj degeneracie");
                return;
            }

            _slotsService.DelayUser(Context.User.Id);

            var result = _slotsService.Slot();
            var emotes = Context.Guild.Emotes.Where(a => !a.Animated).ToList();

            GuildEmote emote1, emote2, emote3;

            if (result == 3)
            {
                emote1 = emotes[_random.Next(emotes.Count - 1)];
                await SendLosujResult(emote1, emote1, emote1);
            }
            else if (result == 2)
            {
                emote1 = emotes[_random.Next(emotes.Count - 1)];
                emotes.Remove(emote1);
                emote2 = emotes[_random.Next(emotes.Count - 1)];
                var emote2Pos = _random.Next(3);
                if (emote2Pos == 0)
                    await SendLosujResult(emote2, emote1, emote1);
                if (emote2Pos == 1)
                    await SendLosujResult(emote1, emote2, emote1);
                if (emote2Pos == 2)
                    await SendLosujResult(emote1, emote1, emote2);
            }
            else
            {
                emote1 = emotes[_random.Next(emotes.Count - 1)];
                emotes.Remove(emote1);
                emote2 = emotes[_random.Next(emotes.Count - 1)];
                emotes.Remove(emote2);
                emote3 = emotes[_random.Next(emotes.Count - 1)];
                await SendLosujResult(emote1, emote2, emote3);
            }
        }

        [RequireOwner]
        [Command("szansa")]
        public async Task ChangeChance(float multiplier)
        {
            _slotsService.ChangeChance(multiplier);
            await Context.Channel.SendMessageAsync("zmienilem se");
        }

        [RequireOwner]
        [Command("losowanie delay")]
        public async Task ChangeDelay(TimeSpan delay)
        {
            _slotsService.ChangeDelay(delay);
            await Context.Channel.SendMessageAsync("zmienilem se");
        }

        private async Task SendLosujResult(GuildEmote emote1, GuildEmote emote2, GuildEmote emote3)
        {
            var addMsg = emote1.Id == emote2.Id
                ? emote1.Id == emote3.Id
                    ? "Brawo xd"
                    : "No prawie"
                : emote1.Id == emote3.Id || emote2.Id == emote3.Id
                    ? "No prawie"
                    : "Przegrałeś xD";

            await Context.Channel.SendMessageAsync($"<:{emote1.Name}:{emote1.Id}><:{emote2.Name}:{emote2.Id}><:{emote3.Name}:{emote3.Id}>");
            await Context.Channel.SendMessageAsync($"{Context.User.Mention}\n{addMsg}");
        }
    }
}
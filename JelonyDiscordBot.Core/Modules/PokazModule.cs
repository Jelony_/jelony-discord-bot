﻿using System.Threading.Tasks;
using Discord.Commands;
using JelonyDiscordBot.Core.Services;

namespace JelonyDiscordBot.Core.Modules
{
    [Group("pokaz"), RequireContext(ContextType.Guild)]
    public class PokazModule : ModuleBase<SocketCommandContext>
    {
        [Command]
        public async Task CheckPokaz(string topic)
        {
            var scraper = new DuckDuckGoScraper();
            
            await using var image = scraper.GetRandomImage(topic);
            await Context.Channel.SendFileAsync(image, "img.jpg");
        }
    }
}
﻿using System.Globalization;

namespace JelonyDiscordBot.Shared.Extensions
{
    public static class StringExtensions
    {
        public static bool EqualsNormalized(this string s1, string s2)
        {
            return string.Compare(s1.ToLowerInvariant(), s2.ToLowerInvariant(), CultureInfo.CurrentCulture, CompareOptions.IgnoreNonSpace) == 0;
        }
    }
}
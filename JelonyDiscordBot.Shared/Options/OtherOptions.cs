﻿using System;

namespace JelonyDiscordBot.Shared.Options
{
    public class OtherOptions
    {
        public const string Key = "Other";
        public ulong VotingServer { get; set; }
        public ulong VotingChannel { get; set; }
        public string VotingServerTimeZone { get; set; }
        public TimeSpan MessageUserDelay { get; set; }
    }
}
namespace JelonyDiscordBot.Shared.Options
{
    public class TemplateRemoverOptions
    {
        public const string Key = "TemplateRemover";
        public int MaxTemplateWidth { get; set; }
        public int MaxTemplateHeight { get; set; }
        public double ValLimit { get; set; }
    }
}
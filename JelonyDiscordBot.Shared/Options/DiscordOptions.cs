﻿namespace JelonyDiscordBot.Shared.Options
{
    public class DiscordOptions
    {
        public const string Key = "Discord";
        public string Token { get; set; }
    }
}
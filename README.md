# JelonyDiscordBot

JelonyBot is a Discord bot with polish commands made for creator's needs. Feel free to check it by yourself. It's made in .NET Core for both: Windows and Linux operating systems as a standalone service.

## Installation

### Windows

Unzip the package for Windows (`windows` is in the name) downloaded here: [Binaries](https://bitbucket.org/Jelony_/jelony-discord-bot/downloads/). Configure the `config/appsettings.json` file (especially `Discord -> Token` field) and using `cmd` in administrator mode execute a following command:

```
JelonyDiscordBot.Service.exe install
```

To uninstall simply execute:

```
JelonyDiscordBot.Service.exe uninstall
```

You can run the service in `Windows Services`.

### Linux (tested on Ubuntu 18.04)

You have to download and install `emgucv` library on the machine. Here's a simple guide, just follow instructions: [Download and Installation - EmguCV](https://www.emgu.com/wiki/index.php/Download_And_Installation#Linux)

Unzip the package for Linux (`linux` is in the name) downloaded here: [Binaries](https://bitbucket.org/Jelony_/jelony-discord-bot/downloads/). Configure the `config/appsettings.json` file (especially `Discord -> Token` field) and copy `emgucv/libs/x64/libcvextern.so` file to unzipped binaries of the JelonyDiscordBot. It should be placed in the root directory of those binaries.

Run the service using (probably you will need to add a permission to execute it with `chmod`):

```
./JelonyDiscordBot.Service
```

Or in the background:

```
screen -S jelony-bot
./JelonyDiscordBot.Service
<click Ctrl+A and Ctrl+D to exit screen>
```

## Update

Shutdown the service, create a backup, replace old binaries with new ones, remember to reconfigure it and run the service.

## Usage

Will be added if the project will be more "universal" because at the current state this bot fulfills only creator's needs.

## License
[MIT](https://choosealicense.com/licenses/mit/)
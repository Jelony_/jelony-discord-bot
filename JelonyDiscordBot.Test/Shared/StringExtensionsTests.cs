﻿using JelonyDiscordBot.Shared.Extensions;
using NUnit.Framework;

namespace JelonyDiscordBot.Test.Shared
{
    public class StringExtensionsTests
    {
        [Test]
        public void NormalizationTest()
        {
            Assert.IsTrue("pokaż".EqualsNormalized("pokaz"));
            Assert.IsTrue("pokaz".EqualsNormalized("pokaż"));
        }
    }
}
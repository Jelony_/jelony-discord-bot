﻿using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

namespace JelonyDiscordBot.Service
{
    class Program
    {
        public static async Task Main(string[] args)
        {
            var pathToExe = Process.GetCurrentProcess().MainModule?.FileName;
            var pathToContentRoot = Path.GetDirectoryName(pathToExe);
            Directory.SetCurrentDirectory(pathToContentRoot);

            await new HostBuilder(args).Build().RunAsync();
        }
    }
}

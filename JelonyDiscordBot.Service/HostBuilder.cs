﻿using System;
using System.IO;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using JelonyDiscordBot.Core;
using JelonyDiscordBot.Core.Factories;
using JelonyDiscordBot.Core.Modules;
using JelonyDiscordBot.Core.Providers;
using JelonyDiscordBot.Core.Services;
using JelonyDiscordBot.Shared.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace JelonyDiscordBot.Service
{
    public class HostBuilder
    {
        private readonly IHostBuilder _hostBuilder;
        public HostBuilder(string[] args)
        {
            _hostBuilder = Host.CreateDefaultBuilder(args)
                .UseSystemd()
                .ConfigureAppConfiguration(ConfigureAppConfiguration)
                .ConfigureServices(ConfigureServices)
                .ConfigureLogging(ConfigureLogging);
        }

        public IHost Build() => _hostBuilder.Build();

        private void ConfigureAppConfiguration(HostBuilderContext context, IConfigurationBuilder config)
        {
            config.SetBasePath(Path.Combine(Directory.GetCurrentDirectory(), "config"))
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{context.HostingEnvironment.EnvironmentName}.json", optional: true, reloadOnChange: true);
        }

        private void ConfigureServices(HostBuilderContext context, IServiceCollection services)
        {
            services.Configure<DiscordOptions>(context.Configuration.GetSection(DiscordOptions.Key));
            services.Configure<TemplateRemoverOptions>(context.Configuration.GetSection(TemplateRemoverOptions.Key));
            services.Configure<OtherOptions>(context.Configuration.GetSection(OtherOptions.Key));

            services.AddSingleton<ITemplateProvider, TemplateProvider>();
            services.AddSingleton<IJobIdProvider, JobIdProvider>();
            services.AddSingleton<IPrefixProvider, PrefixProvider>();

            services.AddSingleton<ITemplateRemoverFactory, TemplateRemoverFactory>();
            services.AddTransient<TemplateRemover>().AddTransient<ITemplateRemover, TemplateRemover>(s => s.GetService<TemplateRemover>());
            services.AddSingleton<ISlotsService, SlotsService>();
            services.AddSingleton<IVotedUsersService, VotedUsersService>();

            services.AddSingleton<INonCommandMessageHandler, NonCommandMessageHandler>();

            services.AddSingleton(new DiscordSocketClient(new DiscordSocketConfig
            {
                GatewayIntents = GatewayIntents.All
            }));
            services.AddSingleton(new CommandService());
            services.AddSingleton<CommandHandler>();
            services.AddSingleton<TemplateRemoveModule>();

            services.AddHostedService<BotClient>();
            services.AddHostedService<BotWorker>();
        }

        private void ConfigureLogging(HostBuilderContext context, ILoggingBuilder logging)
        {
            logging.SetMinimumLevel(context.HostingEnvironment.IsDevelopment() ? LogLevel.Trace : LogLevel.Information);
        }
    }
}
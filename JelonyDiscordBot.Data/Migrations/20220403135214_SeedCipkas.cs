﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JelonyDiscordBot.Data.Migrations
{
    public partial class SeedCipkas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Cipkas",
                columns: new[] { "CipkaId", "Name" },
                values: new object[] { 1, "cipka" });

            migrationBuilder.InsertData(
                table: "Cipkas",
                columns: new[] { "CipkaId", "Name" },
                values: new object[] { 20, "pizda" });

            migrationBuilder.InsertData(
                table: "Cipkas",
                columns: new[] { "CipkaId", "Name" },
                values: new object[] { 19, "pusia" });

            migrationBuilder.InsertData(
                table: "Cipkas",
                columns: new[] { "CipkaId", "Name" },
                values: new object[] { 18, "cip cip cip" });

            migrationBuilder.InsertData(
                table: "Cipkas",
                columns: new[] { "CipkaId", "Name" },
                values: new object[] { 17, "cimpek" });

            migrationBuilder.InsertData(
                table: "Cipkas",
                columns: new[] { "CipkaId", "Name" },
                values: new object[] { 16, "cipuszka" });

            migrationBuilder.InsertData(
                table: "Cipkas",
                columns: new[] { "CipkaId", "Name" },
                values: new object[] { 15, "pipeczka" });

            migrationBuilder.InsertData(
                table: "Cipkas",
                columns: new[] { "CipkaId", "Name" },
                values: new object[] { 14, "dziureczka" });

            migrationBuilder.InsertData(
                table: "Cipkas",
                columns: new[] { "CipkaId", "Name" },
                values: new object[] { 13, "muszelka" });

            migrationBuilder.InsertData(
                table: "Cipkas",
                columns: new[] { "CipkaId", "Name" },
                values: new object[] { 12, "grażynka" });

            migrationBuilder.InsertData(
                table: "Cipkas",
                columns: new[] { "CipkaId", "Name" },
                values: new object[] { 11, "dorotka" });

            migrationBuilder.InsertData(
                table: "Cipkas",
                columns: new[] { "CipkaId", "Name" },
                values: new object[] { 10, "pipka" });

            migrationBuilder.InsertData(
                table: "Cipkas",
                columns: new[] { "CipkaId", "Name" },
                values: new object[] { 9, "cipeczka" });

            migrationBuilder.InsertData(
                table: "Cipkas",
                columns: new[] { "CipkaId", "Name" },
                values: new object[] { 8, "cipa" });

            migrationBuilder.InsertData(
                table: "Cipkas",
                columns: new[] { "CipkaId", "Name" },
                values: new object[] { 7, "cipolinek" });

            migrationBuilder.InsertData(
                table: "Cipkas",
                columns: new[] { "CipkaId", "Name" },
                values: new object[] { 6, "cipiripi" });

            migrationBuilder.InsertData(
                table: "Cipkas",
                columns: new[] { "CipkaId", "Name" },
                values: new object[] { 5, "cipusieńka" });

            migrationBuilder.InsertData(
                table: "Cipkas",
                columns: new[] { "CipkaId", "Name" },
                values: new object[] { 4, "cipuleńka" });

            migrationBuilder.InsertData(
                table: "Cipkas",
                columns: new[] { "CipkaId", "Name" },
                values: new object[] { 3, "cipucha" });

            migrationBuilder.InsertData(
                table: "Cipkas",
                columns: new[] { "CipkaId", "Name" },
                values: new object[] { 2, "cipewa" });

            migrationBuilder.InsertData(
                table: "Cipkas",
                columns: new[] { "CipkaId", "Name" },
                values: new object[] { 21, "pizdeczka" });

            migrationBuilder.InsertData(
                table: "Cipkas",
                columns: new[] { "CipkaId", "Name" },
                values: new object[] { 22, "pizdolinka" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Cipkas",
                keyColumn: "CipkaId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Cipkas",
                keyColumn: "CipkaId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Cipkas",
                keyColumn: "CipkaId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Cipkas",
                keyColumn: "CipkaId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Cipkas",
                keyColumn: "CipkaId",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Cipkas",
                keyColumn: "CipkaId",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Cipkas",
                keyColumn: "CipkaId",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Cipkas",
                keyColumn: "CipkaId",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Cipkas",
                keyColumn: "CipkaId",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Cipkas",
                keyColumn: "CipkaId",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Cipkas",
                keyColumn: "CipkaId",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Cipkas",
                keyColumn: "CipkaId",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Cipkas",
                keyColumn: "CipkaId",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Cipkas",
                keyColumn: "CipkaId",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Cipkas",
                keyColumn: "CipkaId",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Cipkas",
                keyColumn: "CipkaId",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Cipkas",
                keyColumn: "CipkaId",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Cipkas",
                keyColumn: "CipkaId",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Cipkas",
                keyColumn: "CipkaId",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Cipkas",
                keyColumn: "CipkaId",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Cipkas",
                keyColumn: "CipkaId",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Cipkas",
                keyColumn: "CipkaId",
                keyValue: 22);
        }
    }
}

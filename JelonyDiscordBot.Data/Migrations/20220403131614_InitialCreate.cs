﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JelonyDiscordBot.Data.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CipkaDisabledChannels",
                columns: table => new
                {
                    CipkaDisabledChannelId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ChannelId = table.Column<ulong>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CipkaDisabledChannels", x => x.CipkaDisabledChannelId);
                });

            migrationBuilder.CreateTable(
                name: "Cipkas",
                columns: table => new
                {
                    CipkaId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cipkas", x => x.CipkaId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CipkaDisabledChannels");

            migrationBuilder.DropTable(
                name: "Cipkas");
        }
    }
}

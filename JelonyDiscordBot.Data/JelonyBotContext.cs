﻿using System;
using JelonyDiscordBot.Data.Entity;
using Microsoft.EntityFrameworkCore;

namespace JelonyDiscordBot.Data
{
    public class JelonyBotContext : DbContext
    {
        public DbSet<Cipka> Cipkas { get; set; }
        public DbSet<CipkaDisabledChannel> CipkaDisabledChannels { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite($"Data Source=jelony.db");
    }
}
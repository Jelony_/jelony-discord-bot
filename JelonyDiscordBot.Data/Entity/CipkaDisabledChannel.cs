﻿namespace JelonyDiscordBot.Data.Entity
{
    public class CipkaDisabledChannel
    {
        public int CipkaDisabledChannelId { get; set; }
        public ulong ChannelId { get; set; }
    }
}